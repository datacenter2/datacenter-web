# Datacenter case

The company **Data AI** is planning the build of a datacenter involving the implementation and maintenance of: 

* Cooling 
* Power
* Ventilation

**DataAI** consists of team’s with competencies in IT Technology and Maintenance engineering. As part of the datacenter build, their purpose is to develop a standalone monitoring system to prevent breakdowns. Data AI will at project start define key performance indicators (KPI) and work towards these during the project.

Breakdown risks are: 

* Fire
* Loss of cooling
* Loss of power 
* Deviations in relative air humidity 

The monitoring system consists of several sensors that continuously monitors and persists above mentioned risks.  

Data is presented in a maintenance dashboard.  

A very simplified overview of the system is shown in the below block diagram.


<img src="./images/project_overview_datacenter2.png" width="1000" alt="datacenter system overview">

Furthermore, Data AI would like to offer services to the customers of the datacenter to give them information regarding the operation of the datacenter. The information should include data regarding the risks mentioned above, but also data regarding the customer's usage (actual, historical, average, and peak) of resources (storage, data traffic and so forth).  
2 levels of access is needed, confidential and public.
