# Links

## Project management
* [Scrum.org](https://www.scrum.org/)

## Team building
* [Tuckman's model](https://www.teambuildingactivity.com/tuckmans-model/)  
* [IT Technology team building](https://eal-itt.gitlab.io/collaboration-days/team_building/)

## IT Technology ressources
* [Weekly plans and exercises](https://eal-itt.gitlab.io/21s-itt2-project)
* [Project companion site](https://eal-itt.gitlab.io/datacenter-iot)
* [Gitlab student project group](https://gitlab.com/21s-itt2-datacenter-students-group)